import React, {useState} from "react";
import { Route, Routes } from "react-router-dom";
import Home from "./Pages/Home/Home";
import Quiz from "./Pages/Quiz/Quiz";
import Results from "./Pages/Results/Results";

function App() {
  const [data, setData] = useState();
  const [score, setScore] = useState([]);

  return (
    <Routes>
      <Route path="/" element={<Home />} />;
      <Route path="/quiz" element={<Quiz setScore={setScore} score={score} setData={setData} data={data} />} />;
      <Route path="/results" element={<Results score={score} />} />;
    </Routes>
  );
}

export default App;
