import React from 'react';
import Button from '../../components/Button/Button';
import styles from "./Home.module.css";


function Home() {

  return (
    <div className={styles.ctn}>
      <h1 className={styles.title}>Welcome to the Trivia Challenge!</h1>
      <h2>You will be presented with 10 True or False questions.</h2>
      <h2 className={styles.lightBlueColor}>Can you score 100%?</h2>
      <Button title="Begin" goTo="/quiz" />
    </div>
  );
}

export default Home;
