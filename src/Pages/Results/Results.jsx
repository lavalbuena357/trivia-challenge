import React from 'react';
import Button from '../../components/Button/Button';
import styles from './Results.module.css';

function Results({score}) {

  const correctAnswers = score.map(el => el.result).reduce((cur, acum) => cur+acum)
  
  return (
    <div className={styles.ctn}>
      <div>
        <h1 className={styles.title}>You scored</h1>
        <h2 className={styles.score}>{correctAnswers}/{score.length}</h2>
      </div>
      <div className={styles.resultCtn}>
        <ul>
        {score.map((el,i) => (
          <li 
            key={i} 
            className={el.result === 1 ? styles.correct : styles.error}
          >{i+1} {el.question}</li>
        ))}
        </ul>
        
      </div>
      <Button title='PLAY AGAIN' goTo='/' />
    </div>
  );
}

export default Results;
