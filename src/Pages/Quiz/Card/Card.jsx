import React from 'react';
import { useNavigate } from 'react-router-dom';
import styles from './Card.module.css';

function Card({currentQ, setScore, score, setCurrent, current, data}) {

  const questionReplace = currentQ && currentQ.question.replace(/&quot;/g,'"').replace(/&#039;/g,"'")

  let navigate = useNavigate()

  const result = (e) => {
    const {value} = e.target
    if(currentQ && currentQ.correct_answer === value) {
      setScore([...score, {question: questionReplace, result: 1}])
    } else {
      setScore([...score, {question: questionReplace, result: 0}])
    }
    setCurrent(++current)

    stopQuiz()
  }

  const stopQuiz = () => {
    if(current > data.length-1) {
      navigate({pathname: '/results'})
    }
  }

  return (
    <div className={styles.ctn}>
      <h2>{currentQ && currentQ.category}</h2>
      <div className={styles.questionBox}>
        <p>{questionReplace}</p>
        <div className={styles.buttonCtn}>
          <button value="True" onClick={result}>True</button>
          <button value="False" onClick={result}>False</button>
        </div>
      </div>
    </div>
  )
}

export default Card
