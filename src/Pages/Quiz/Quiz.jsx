import React, { useEffect, useState } from 'react';
import petition from '../../api/petition';
import Card from './Card/Card';
import Loader from '../../components/Loader/Loader';
import styles from './Quiz.module.css';

function Quiz({setScore, score, setData, data}) {
  const [current, setCurrent] = useState(0);
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    (async() => {
      const init = await petition('questions');
      if(init && init.response_code === 0) {
        setData(init.results);
        setIsLoading(false)
      }
    })();
    setScore([])
  }, [])


  return (
    <div className={styles.ctn}>
      {isLoading ? 
        <Loader />
        :
        <div className={styles.box}>
          <h1 className={styles.title}>Trivia Challenge</h1>
          <ul className={styles.guidePoints}>
            {data && data.map((el, i) => (
              <li 
                key={i} 
                className={i <= current ? styles.passed : styles.pending}
              ></li>
            ))}
          </ul>
          <Card 
            setCurrent={setCurrent}
            current={current}
            data={data && data}
            currentQ={data && data[current]} 
            setScore={setScore} 
            score={score}
          />
          <h3>{`${current + 1}/${data && data.length}`}</h3>
        </div>
      }
    </div>
  );
}

export default Quiz;
