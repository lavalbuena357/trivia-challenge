const { REACT_APP_API_URL } = process.env;

const services = () => {
  return {
    questions: {
      server: REACT_APP_API_URL,
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin' : '*',
        'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Allow-Credentials' : 'true'
      }
    }
  };
};

export default services;