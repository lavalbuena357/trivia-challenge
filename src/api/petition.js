import services from "./services";

const petition = async(service, endpoint, method='GET', data={}) => {
  let json;
  try {
    if(services().hasOwnProperty(service)) {

      const {server, headers} = services()[service]
  
      const url = endpoint ? `${server}/${endpoint}` : server

      const config = {
        method,
        headers: headers,
      };

      if (method === 'POST' || method === 'PUT') {
        const response = await fetch(url, config);
        json = await response.json()
        return json;
      } else if(method === 'GET') {
        const res = await fetch(url)
        json = res.json()
        return json;
      }
    }
  } catch (error) {console.error(error)}
}

export default petition;