import React from 'react';
import { Link } from 'react-router-dom';
import styles from './Button.module.css'

function Button({title, goTo}) {
  return (
    <div className={styles.ctn}>
      <Link to={goTo}>{title}</Link>
    </div>
  );
}

export default Button;
