import React from 'react'

function IsLoading() {
  return (
    <div>
      Loading data...
    </div>
  )
}

export default IsLoading
